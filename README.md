# Challenge GD4H - Carto'Ambroisies

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Elle organise un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées.

Liens : 
<a href="https://challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Site</a> / 
<a href="https://forum.challenge.gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Forum</a>
a

## Carto'Ambroisies

Les Ambroisies sont des plantes très invasives. Une fois qu’un pied d’ambroisie est observé, il faut rapidement l’éliminer car il est difficile de l’éradiquer une fois qu’il est installé.

Or, près de 3% de la population française serait allergique à l’Ambroisie, selon l’Anses. Le pollen de cette plante peut, en fin d’été, provoquer de fortes réactions allergiques.

Aussi, sa capacité à produire de grande quantité de graines en fait également une menace pour l’agriculture et pour la biodiversité.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=32" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

>TODO / **Description Solution**

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

>TODO / **documentation d'utilisation de la solution**

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).