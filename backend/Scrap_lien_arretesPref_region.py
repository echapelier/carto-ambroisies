import pandas as pd
import requests
from bs4 import BeautifulSoup


lien_page_arretes = "https://ambroisie-risque.info/reglementation/#les-arretes-prefectoraux-par-departement"

def LienArrete(): 

    page_liste_arretes = requests.get(lien_page_arretes)
    soup = BeautifulSoup(page_liste_arretes.content, "html.parser")
    table = soup.find('table', class_='spip')
    rows = table.find_all('tr')[1:]

    liste_dpts_avec_arrete = pd.DataFrame([
        {
            "departements-noms": columns[0].text.strip(),
            "Date": columns[1].text.strip(),
            "Arrêté": columns[2].find('a').text.strip() if columns[2].find('a') else '',
            "Arrêté Lien": columns[2].find('a')['href'] if columns[2].find('a') else '',
            "Plan action": columns[3].find('a').text.strip() if columns[3].find('a') else '',
            "Plan action lien": columns[3].find('a')['href'] if columns[3].find('a') else ''
        }
        for row in rows
        if (columns := row.find_all('td'))
    ])

    liste_complete_dpts = pd.read_json("data/listesimplifiee-departements.json")
    liste_complete_dpts = pd.json_normalize(liste_complete_dpts["features"])
    liste_complete_dpts = liste_complete_dpts[["properties.departements-numeros", "properties.departements-noms"]]
    liste_complete_dpts.rename(columns={"properties.departements-numeros": "departements-numeros", "properties.departements-noms": "departements-noms"}, inplace=True)

    liste_dpts_avec_arrete = pd.merge(liste_dpts_avec_arrete, liste_complete_dpts, how="inner", on="departements-noms")

    liste_dpts_avec_arrete.to_csv("dpts_lien_arrete.csv", sep=";", encoding="utf-8", index=False)

LienArrete()