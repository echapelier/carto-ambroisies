#Importation des librairies
import requests as r
from bs4 import BeautifulSoup
import json
import pandas as pd
import random

# Lien vers la carte vigilance pollen du site Pollen.fr
lienMapPollen = "https://www.pollens.fr/load_vigilance_map"


# Fonction scrap du risque pollen de l'Ambroisie par régions et transforme en tableau.
# Ajout d'une colonnes risque avec des données aléatoires pour la phase PoC.

def PollenRisk():
    response = r.get(lienMapPollen)
    soup = BeautifulSoup(response.content, 'html.parser')
    soupText = soup.text
    pollenDataJson = json.loads(soupText)
    pollenData = json.loads(pollenDataJson["vigilanceMapCounties"])
    county_list = []

    for county_num, county_info in pollenData.items():
        county_dict = {}
        county_dict['countyNumber'] = county_num
        county_dict['countyName'] = county_info['countyName']
        county_dict['riskLevel'] = county_info['riskLevel']
        
        for risk in county_info['risks']:
            county_dict[risk['pollenName']] = risk['level']
        
        county_list.append(county_dict)

    df = pd.DataFrame(county_list)

    df["AmbroisiesRandomSample"] = df.apply(lambda row: random.randint(0, 3), axis=1)

    return df[["countyNumber", "countyName", "Ambroisies", "AmbroisiesRandomSample"]]

RisquePollenAmbroisie = PollenRisk()