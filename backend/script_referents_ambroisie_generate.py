import os
import geopandas as gpd
import pandas as pd
from pyproj import Transformer


def generate_referents_metropole():
    communes_fr = pd.read_csv("data/georef-france-commune.csv",
                 sep=";",
                 header=0,
                 usecols=[0,17],
                 names=["geom_point", "code_insee"],
                 dtype={"code_insee": 'str'}
                )
    communes_fr = (communes_fr
                   .sort_values(by="code_insee", ascending=True)
                   .reset_index(drop=True).copy()
                   .assign(lon=lambda x: (x["geom_point"].str.split(",", expand=True)[[0]]),
                           lat=lambda x: (x["geom_point"].str.split(",", expand=True)[[1]]))
                   )
    
    
    referents_df = pd.read_csv("data/referents_communaux_mai_23.csv",
                        sep=";",
                        dtype=({"code_insee": str})
                       )
    
    referents_df = pd.merge(referents_df, communes_fr, on="code_insee", how="left")
    
    transformer = Transformer.from_crs("4326", "2154")
    
    referents_df = (referents_df
                    .assign(lon=transformer.transform(referents_df.lon, referents_df.lat)[0],
                            lat=transformer.transform(referents_df.lon, referents_df.lat)[1])
                    .drop(columns = "geom_point")
                   )
    
    
    gdf = gpd.GeoDataFrame(referents_df, crs = "EPSG:2154", geometry=gpd.points_from_xy(referents_df.lon, referents_df.lat))
    
    gdf = gdf.drop(columns=["lon", "lat"]).copy()
    
    gdf.to_file("referents_metropole_generated.geojson", driver='GeoJSON')

generate_referents_metropole()
