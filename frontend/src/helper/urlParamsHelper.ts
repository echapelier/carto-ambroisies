// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function urlParamsHelper(params: any): URLSearchParams {
  const search = new URLSearchParams();
  if (params) {
    for (const paramKey in params) {
      if (Array.isArray(params[paramKey])) {
        for (const paramItem of params[paramKey]) {
          if (paramItem != undefined) {
            search.append(`${paramKey}[]`, paramItem);
          }
        }
      } else {
        if (params[paramKey] != undefined) {
          search.append(paramKey, params[paramKey]);
        }
      }
    }
  }
  return search;
}
