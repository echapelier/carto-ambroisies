/* eslint-disable @typescript-eslint/no-explicit-any */

import { GeoJsonObject } from "geojson";

//import { Feature } from "geojson";
export interface ICoordinates {
  lat: number;
  lng: number;
}

export class City {
  // implements IDicoType {
  public code = "";
  public name = "";
  public departement_nom = "";
  public departement_code = 0;
  public context = "";
  public coordinates: ICoordinates | null = null;
  public geojson: GeoJsonObject | null | undefined = null;

  constructor(
    properties: any,
    center?: any,
    bounds?: any,
    geojson?: GeoJsonObject
  ) {
    if (properties != null) {
      this.code = properties.citycode ? properties.citycode : properties.code;
      this.name = properties.label ? properties.label : properties.nom;
      // le contexte de l'api geo est de la forme 03, Allier, Auvergne-Rhône-Alpes
      if (properties.departement == null && properties.context != null) {
        const depts = properties.context.split(",");
        if (depts.length > 2) {
          this.departement_code = depts[0];
          this.departement_nom = depts[1];
        }
      }
      if (properties.departement != null) {
        this.departement_code = properties.departement.code;
        this.departement_nom = properties.departement.nom;
      }

      this.context = properties.context
        ? properties.context
        : properties.departement.code +
          ", " +
          properties.departement.nom +
          ", " +
          properties.region.nom;
    }
    if (center != null && center.coordinates != null) {
      this.coordinates = {
        lat: center.coordinates[1],
        lng: center.coordinates[0],
      };
    }
    console.log("geojson", geojson);
    this.geojson = geojson;
  }
}
