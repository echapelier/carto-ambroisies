import { ref } from "vue";
import { defineStore } from "pinia";
import { fetchDepartments } from "@/api/department";
import { fetchPreviousByDepartment } from "@/api/history";
import L, { Layer } from "leaflet";
import { Feature } from "geojson";

// return await fetchDepartments().then((res) => {
//   res.features.map((f) => {
//     if (f.properties != null) {
//       // ICI possibilité d'ajouter des propriétés supplémentaire
//       f.properties["toto"] = 3;
//       return f;
//     }
//   });
//   return res;
// });

export const useDepartmentsStore = defineStore("departments", () => {
  const isloaded = ref(false);
  const toDisplay = ref(false);
  const layer = ref();
  const data = ref();
  const minZoom = 1;
  const maxZoom = 7;
  const currentYear = ref(2022);

  const historyData = ref();
  const isHistoryDataLoaded = ref(false);

  const changeYear = (year: number) => {
    if (!isloaded.value) return;
    currentYear.value = year;

    layer.value.setStyle(function (feature: Feature) {
          return {
              weight: 0.6,
              opacity: 0.7,
              color: "grey",
              fillOpacity: getOpacity(feature?.properties?feature?.properties[currentYear.value]: null),
              fillColor: getColor(feature?.properties?feature?.properties[currentYear.value]: null),
            };
    });

    // layer.value = L.geoJson(data.value, {
    //   style: function (feature) {
    //     return {
    //       weight: 0.6,
    //       opacity: 0.7,
    //       color: "grey",
    //       fillOpacity: getOpacity(feature?.properties[currentYear.value]),
    //       fillColor: getColor(feature?.properties[currentYear.value]),
    //     };
    //   },
    // });
    console.log("changeYear", layer.value);
  };

  const load = async () => {
    fetchPreviousByDepartment()
      .then((response: any) => {
        historyData.value = response;
      })
      .then(() => {
        fetchDepartments().then((res) => {
          isloaded.value = false;
          data.value = res;
          const polystyle = {
            weight: 0.6,
            opacity: 1,
            color: "grey",
            fillOpacity: 0,
          };

          data.value.features.map((f: any) => {
            if (f.properties != null && historyData.value != null) {
              // ICI possibilité d'ajouter des propriétés supplémentaire
              const histos = historyData.value.filter(
                (h: any) => h.c_code_dpt == f.properties["departements-numeros"]
              );
              histos.forEach((histo: any) => {
                f.properties[histo.Annee] = histo.counts;
              });
              return f;
            }
          });
          console.log("data.value", data.value);
          layer.value = L.geoJson(res, {
            style: function (feature) {
              return {
                weight: 0.6,
                opacity: 0.7,
                color: "grey",
                fillOpacity: getOpacity(feature?.properties[currentYear.value]),
                fillColor: getColor(feature?.properties[currentYear.value]),
              };
            },
          });
          isloaded.value = true;
          toDisplay.value = true;
        });
      });
    // await loadHistoryData().then((r) => {
    // console.log("r", r);

    //});
  };

  function getOpacity(d: number) {
    return d == 0 || d == null ? 0 : 0.7;
  }

  function getColor(d: number) {
    return d == 0 || d == null
      ? ""
      : d > 100
      ? "#800026"
      : d > 80
      ? "#BD0026"
      : d > 60
      ? "#E31A1C"
      : d > 40
      ? "#FC4E2A"
      : d > 20
      ? "#FD8D3C"
      : "#FFEDA0";
  }

  const loadHistoryData = () => {
    if (!isHistoryDataLoaded.value) {
      fetchPreviousByDepartment().then((response: any) => {
        console.log("fetchPreviousByDepartment", response);
        historyData.value = response.data;
      });
      isHistoryDataLoaded.value = true;
    }
  };

  return {
    load,
    isloaded,
    toDisplay,
    layer,
    data,
    minZoom,
    maxZoom,
    currentYear,
    changeYear,
  };
});
