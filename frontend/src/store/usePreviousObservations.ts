import { ref } from "vue";
import { defineStore } from "pinia";
import { fetchPrevious } from "@/api/history";
import { Feature } from "geojson";
import L from "leaflet";
import {
  iconeArmoiseHisto,
  iconeTrifideHisto,
  iconeEpislissesHisto,
} from "@/leaflet/icon/ambroisieIcon";
import proj4 from "proj4";
// Les éléments proj4leaflet sont-ils nécessaires ?
import "proj4leaflet";
import { Proj4GeoJSONFeature } from "proj4leaflet";

// import proj4 from "proj4";
// import "proj4leaflet";
// import * as geojson from "geojson";

export const usePreviousObservationsStore = defineStore(
  "previousObservations",
  () => {
    const minZoom = 12;
    const maxZoom = 19;
    const isloaded = ref(false);
    const toDisplay = ref(false);
    const layer = ref();
    const data = ref();
    const currentYear = ref(2022);

    const changeYear = (year: number) => {
      console.log("changeYear", year);
      currentYear.value = year;
      layer.value.setStyle(function (feature: Feature) {
        return {
          opacity: getOpacity(
            feature?.properties ? feature?.properties.Annee : null
          ),
          fillOpacity: getOpacity(
            feature?.properties ? feature?.properties.Annee : null
          ),
        };
      });
    };
    function getOpacity(d: number) {
      const retour = d == 0 || d == null || d != currentYear.value ? 0 : 1;
      const value = (currentYear.value - d) / 100;
      return value;
    }
    const load = () => {
      fetchPrevious(currentYear.value).then((res) => {
        console.log("fetchPrevious", res);
        isloaded.value = false;
        proj4.defs(
          "EPSG:2154",
          "+proj=lcc +lat_0=46.5 +lon_0=3 +lat_1=49 +lat_2=44 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"
        );
        data.value = res;
        layer.value = L.Proj.geoJson(res, {
          // filter: function (feature) {
          //   return feature.properties.Annee == currentYear.value;
          // },
          style: function (feature) {
            return {
              opacity: getOpacity(
                feature?.properties ? feature?.properties.Annee : null
              ),
              fillOpacity: getOpacity(
                feature?.properties ? feature?.properties.Annee : null
              ),
            };
          },
          pointToLayer: function (feature, latlng) {
            if (feature.properties.c_espece.includes("armoise")) {
              return new L.Marker(latlng, {
                icon: iconeArmoiseHisto,
              }) as L.Layer;
            } else if (feature.properties.c_espece.includes("trifide")) {
              return new L.Marker(latlng, {
                icon: iconeTrifideHisto,
              }) as L.Layer;
            } else {
              return new L.Marker(latlng, {
                icon: iconeEpislissesHisto,
              }) as L.Layer;
            }
          },
          onEachFeature: function (feature, layer) {
            layer.bindPopup(
              "<h3>" +
                feature.properties.c_espece +
                "</h3>" +
                "<p>Densité : " +
                feature.properties.c_densite +
                "</p>" +
                "<p>Milieu : " +
                feature.properties.c_milieu +
                "</p>" +
                "<p>Signalé le " +
                new Date(
                  feature.properties.c_date_sb_data_1
                ).toLocaleDateString("fr") +
                "</p>"
            );
          },
        });
        isloaded.value = true;
      });
    };
    return {
      load,
      changeYear,
      isloaded,
      toDisplay,
      layer,
      data,
      minZoom,
      maxZoom,
    };
  }
);
