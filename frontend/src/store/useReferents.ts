import { ref } from "vue";
import { defineStore } from "pinia";
import { fetchReferents } from "@/api/referent";
import { City } from "@/types/city";

export const useReferentsStore = defineStore("referents", () => {
  const isloaded = ref(false);
  const referentData = ref();

  const  loadReferents = async () => {
    if (!isloaded.value) {
      referentData.value = await fetchReferents();
      isloaded.value = true;
    }
  };

  const hasReferentInCity = (city: City): boolean => {
    // Chargement si besoin
     loadReferents();
    const result = referentData.value.find((r:any) => r.code_insee == city.code);
    return (result != null);
  };
  return {
    loadReferents,
    hasReferentInCity,
    isloaded,
    referentData,
  };
});
