import { ref } from "vue";
import { defineStore } from "pinia";
import { fetchRisquePollen } from "@/api/risque-pollen";

export const useRisquePollenStore = defineStore(
  "risquePollen",
  () => {
    const isloaded = ref(false);
    const data = ref();

    const load = async () => {
      if (!isloaded.value) {
        data.value = await fetchRisquePollen();
       }
    };

    const nivRisquePollen = (codeDepartement: number): number => {
      // Chargement si besoin
      load();
      const result = data.value.find(
        (r: any) => r["countyNumber"] == codeDepartement
      );
      return result.AmbroisiesRandomSample;
    };

    return {
      load,
      nivRisquePollen,
      isloaded,
      data,
    };
  }
);
