import { ref } from "vue";
import { defineStore } from "pinia";
import { fetchPrefectoralDecrees } from "@/api/prefectural-decree";
// import { City } from "@/types/city";

export const usePrefecturalDecreesStore = defineStore(
  "prefecturalDecrees",
  () => {
    const isloaded = ref(false);
    const data = ref();

    const load = async () => {
      console.log("arrétés load");
      if (!isloaded.value) {
        data.value = await fetchPrefectoralDecrees();
       }
    };

    const hasInDepartment = (codeDepartement: number): boolean => {
      // Chargement si besoin
      load();
      const result = data.value.find(
        (r: any) => r["departements-numeros"] == codeDepartement
      );
      return result != null;
    };
    return {
      load,
      hasInDepartment,
      isloaded,
      data,
    };
  }
);
