import * as L from "leaflet";
import { City } from "@/types/city";
import { useReferentsStore } from "@/store/useReferents";
import { usePrefecturalDecreesStore } from "@/store/usePrefecturalDecrees";
import { useRisquePollenStore } from "@/store/useRisquePollen";
import "../leaflet/infoCustomControl.css";
import { getInfosMairie_By_Code } from "@/api/infosmairie";

export class InfoCustomControl extends L.Control {
  public info = "";
  public city: City | null | undefined = null;
  public hasReferent: boolean | null = null;
  public hasDecree: boolean | null = null;
  public nivRisquePollen: number | null = null;

  onAdd(map: L.Map) {
    const div = L.DomUtil.create("div");
    L.DomEvent.disableClickPropagation(div);
    L.DomEvent.disableScrollPropagation(div);
    div.classList.add("infoControl");
  
    if (this.city != null) {
      const referentsStore = useReferentsStore();
      const prefecturalDecreesStore = usePrefecturalDecreesStore();
      const risquePollenStore = useRisquePollenStore();
      this.hasReferent =  referentsStore.hasReferentInCity(this.city);
      this.hasDecree =  prefecturalDecreesStore.hasInDepartment(this.city.departement_code);
      this.nivRisquePollen = risquePollenStore.nivRisquePollen(this.city.departement_code);
    }
  
    if (this.city != null) {
      const communeName = document.createElement("p");
      communeName.textContent = `Commune de ${this.city.name}`;
      communeName.classList.add("commune-name");
      div.appendChild(communeName);

      const departementName = document.createElement("p");
      departementName.textContent = `Département ${this.city.departement_nom}`;
      departementName.classList.add("departement-name");
      div.appendChild(departementName);
  
      const referentPresence = document.createElement("p");
      referentPresence.textContent = `Présence référent : ${this.hasReferent ? "OUI" : "NON"}`;
      referentPresence.classList.add("referent-presence");
      div.appendChild(referentPresence);

      const linkArrete = document.createElement("a");
      linkArrete.href = "https://ambroisie-risque.info/reglementation/#les-arretes-prefectoraux-par-departement";
      linkArrete.target = "_blank";
      linkArrete.text = " (Détails) ";

      const presenceArrete = document.createElement("p");
      presenceArrete.textContent = `Présence arrêté : ${this.hasDecree ? "OUI" : "NON"}`;
      presenceArrete.classList.add("presence-arrete");
      if (this.hasDecree) presenceArrete.appendChild(linkArrete);
      div.appendChild(presenceArrete);

      const impactSanitaire = document.createElement("p");
      impactSanitaire.textContent = `Impact sanitaire prévisionnel des pollens : `;
      if (this.nivRisquePollen == 1) {
        impactSanitaire.insertAdjacentHTML('beforeend', `faible <span class="pastille-risque-pollen" style="background-color: #048000;"></span>`);
      } else if (this.nivRisquePollen == 2) {
        impactSanitaire.insertAdjacentHTML('beforeend', `moyen <span class="pastille-risque-pollen" style="background-color: #F2EA1A"></span>`);
      } else if (this.nivRisquePollen == 3) {
        impactSanitaire.insertAdjacentHTML('beforeend', `élevé <span class="pastille-risque-pollen" style="background-color: #FF0200"></span>`)
      } else {
        impactSanitaire.insertAdjacentHTML('beforeend', `nul <span class="pastille-risque-pollen" style="background-color: #FFFFFF"></span>`);
      }
      div.appendChild(impactSanitaire);

      const codeINSEE = this.city.code;
      const infoMairie = document.createElement("p");
      infoMairie.textContent = `Pour en savoir plus, rapprochez-vous de votre mairie.`;
      infoMairie.classList.add("info-mairie");
      div.appendChild(infoMairie);

    } else {
      const welcomeMessage = document.createElement("p");
      welcomeMessage.textContent = "Bienvenue !";
      welcomeMessage.classList.add("welcome-message");
      div.appendChild(welcomeMessage);
  
      const infoMessage = document.createElement("p");
      infoMessage.textContent = "Entrez le nom d’une commune dans la barre de recherche ou cliquez sur la carte pour avoir plus d’informations sur la présence d’ambroisies";
      infoMessage.classList.add("info-message");
      div.appendChild(infoMessage);
    }
  
    return div;
  }

  onRemove(map: L.Map) {
    // Nothing to do here
  }
  constructor(options?: L.ControlOptions, city?: City | null) {
    super(options);
    this.city = city;

    // ICI CHARGER les informations complémentaire (Référent et à partir du départemnt les arrêtés....)
  }
}
