import * as L from "leaflet";
import { useDepartmentsStore } from "@/store/useDepartments";

import "../leaflet/histoCustomControl.css";

export class HistoCustomControl extends L.Control {
  private departmentsStore = useDepartmentsStore();

  public info = "";
  onAdd(map: L.Map) {

    const legendTitle = document.createElement("p");
    legendTitle.innerHTML = "Nombre de signalements d'ambroisie"+ '<br />' + "(toutes espèces confondues)";
    legendTitle.classList.add("legend-title");


    function getColor(d: number) {
    return d == 0 || d == null
      ? ""
      : d > 100
      ? "#800026"
      : d > 80
      ? "#BD0026"
      : d > 60
      ? "#E31A1C"
      : d > 40
      ? "#FC4E2A"
      : d > 20
      ? "#FD8D3C"
      : "#FFEDA0";
    };

    const legend = L.DomUtil.create('div', 'info legend');
    legend.appendChild(legendTitle);
    const grades = [0, 20, 40, 60, 80, 100];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (let i = 0; i < grades.length; i++) {
        legend.innerHTML +=
            '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }
    legend.style.padding = "5px";

    const div = L.DomUtil.create("div");

    /* Prevent click events propagation to map */
    L.DomEvent.disableClickPropagation(div);

    /* Prevent scroll events propagation to map when cursor on the div */
    L.DomEvent.disableScrollPropagation(div);



    // TODO Rendre dynamique la liste des années

    div.innerHTML +=
      "<input type='range' min='2015' max='2022' value='2022' class='slider' id='myRange'>";
    const yearInfo = L.DomUtil.create("p");
    yearInfo.innerHTML = `${useDepartmentsStore().currentYear}`;
    div.appendChild(yearInfo);
    div.appendChild(legend);
    div.style.backgroundColor = "white";
    div.style.boxShadow = "0px 0px 5px 2px rgba(0, 0, 0, 0.5);"
    div.style.borderRadius = "10px"

    const rangeInput = L.DomUtil.get(div)?.children[0] as HTMLInputElement;

    if (rangeInput) {
      L.DomEvent.on(rangeInput, "input", function () {
        console.log("map", map);
        yearInfo.innerHTML = useDepartmentsStore().layer;
        useDepartmentsStore().changeYear(Number(rangeInput.value));
        yearInfo.innerHTML = `${useDepartmentsStore().currentYear}`;
        });
    }

    return div;
  }

  onRemove(map: L.Map) {
    // Nothing to do here
    console.log("onRemove");
  }
  constructor(options?: L.ControlOptions) {
    super(options);

    //   // ICI CHARGER  l'ensemble des dates d'historiqe
  }
}
