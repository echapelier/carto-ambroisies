import * as L from "leaflet";
import { useDepartmentsStore } from "@/store/useDepartments";

export class HistoLegendCustomControl extends L.Control {
  private departmentsStore = useDepartmentsStore();

  public info = "";
  onAdd(map: L.Map) {
      const legendTitle = document.createElement("p");
      legendTitle.innerHTML =
        "Nombre de signalements d'ambroisie" +
        "<br />" +
        "(toutes espèces confondues)";
      legendTitle.classList.add("legend-title");
    
      function getColor(d: number) {
        return d == 0 || d == null
          ? ""
          : d > 100
          ? "#800026"
          : d > 80
          ? "#BD0026"
          : d > 60
          ? "#E31A1C"
          : d > 40
          ? "#FC4E2A"
          : d > 20
          ? "#FD8D3C"
          : "#FFEDA0";
      }
    
      const div = L.DomUtil.create("div", "info legend");
      div.appendChild(legendTitle);
      const grades = [0, 20, 40, 60, 80, 100];
    
      // loop through our density intervals and generate a label with a colored square for each interval
      for (let i = 0; i < grades.length; i++) {
        div.innerHTML +=
          '<i style="background:' +
          getColor(grades[i] + 1) +
          '"></i> ' +
          grades[i] +
          (grades[i + 1] ? "&ndash;" + grades[i + 1] + "<br>" : "+");
      }
    
      return div;
    };

  onRemove(map: L.Map) {
    // Nothing to do here
    console.log("onRemove");
  }
  constructor(options?: L.ControlOptions) {
    super(options);

    //   // ICI CHARGER  l'ensemble des dates d'historiqe
  }
}
