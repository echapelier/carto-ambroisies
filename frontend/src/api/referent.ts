/* eslint-disable @typescript-eslint/no-explicit-any */
import { GeoJsonObject } from "geojson";

import Papa from "papaparse";

export const fetchReferents = () =>
  new Promise((resolve, reject) => {
    Papa.parse(process.env.BASE_URL + "data/referents.csv", {
      header: true,
      download: true,
      complete: function (results) {
        resolve(results.data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });

export const fetchReferentsOld = async (): Promise<GeoJsonObject> => {
  return await fetch(process.env.BASE_URL + "data/referents.geojson", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      return response;
    });
};
