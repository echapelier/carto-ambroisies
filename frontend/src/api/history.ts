/* eslint-disable @typescript-eslint/no-explicit-any */
import { FeatureCollection } from "geojson";
import { fetchDepartments } from "@/api/department";
import Papa from "papaparse";
import { Feature, Point } from "geojson";

export const fetchPreviousByDepartment = () =>
  new Promise((resolve, reject) => {
    Papa.parse(process.env.BASE_URL + `data/ambroisies_count_department.csv`, {
      header: true,
      download: true,
      complete: function (results) {
        resolve(results.data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });

export const fetchPrevious_ByDepartment =
  async (): Promise<FeatureCollection> => {
    // TODO Use Store
    return await fetchDepartments().then((res) => {
      res.features.map((f) => {
        if (f.properties != null) {
          // ICI possibilité d'ajouter des propriétés supplémentaire
          f.properties["toto"] = 3;
          return f;
        }
      });
      return res;
    });
  };
export const fetchPrevious = async (
  year: number
): Promise<Feature<Point, any>> => {
  return await fetch(
    process.env.BASE_URL + `data/signalements_ambroisies_clean_light.geojson`,
    // process.env.BASE_URL + `data/ambroisies_${year}_details.geojson`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      return response;
    });
};
