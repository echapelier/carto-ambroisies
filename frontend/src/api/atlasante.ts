/* eslint-disable @typescript-eslint/no-explicit-any */
import { Feature, Point } from "geojson";
// import * as geojson from "geojson";

const geojsonUrl =
    "https://datacarto.atlasante.fr/wfs/ece0783c-b862-4c6c-8e7b-4a6e689084f3?service=WFS&request=GetFeature&outputformat=geojson&format=text/javascript&version=2.0.0&typename=ms:ambroisie_sb_data_0_diffusion";

export const xhrCurrentObservations = async (): Promise<
  Feature<Point, any>
> => {

  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", geojsonUrl);
    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {

        const data = JSON.parse(xhr.response);

        for (const item of data.features) {
          // Suppression des propriétés non utilisées
          delete item.properties.c__edit_datemaj;
          delete item.properties.c_commune;
          delete item.properties.c_gid;
          delete item.properties.c_photo;
          delete item.properties.c_photo2;
          delete item.properties.c_x;
          delete item.properties.c_y;
          delete item.properties.gid;
          // Rectification des intitulés d'espèce qui affichaient mal accents et apostrophes
          if (item.properties.c_espece.includes("armoise")) {
            item.properties.c_espece = "Ambroisie à feuilles d’armoise";
          };
          if (item.properties.c_espece.includes("lisse")) {
            item.properties.c_espece = "Ambroisie à épis lisse";
          };
        };

        // Filtrage des signalements au statut non validé
        data.features = data.features.filter((item: any) => item.properties.c_statut.includes("validé"))

        resolve(data);
        
      } else {
        reject({
          status: xhr.status,
          statusText: xhr.statusText,
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: xhr.status,
        statusText: xhr.statusText,
      });
    };
    xhr.send();
  });
};

// Tout ce qui suit semble pouvoir être supprimé ?

// export const getCurrentObservations = (): string => {
//   const geojsonUrl =
//     "https://datacarto.atlasante.fr/wfs/ece0783c-b862-4c6c-8e7b-4a6e689084f3?service=WFS&request=GetFeature&outputformat=geojson&format=text/javascript&version=2.0.0&typename=ms:ambroisie_sb_data_0_diffusion";

//   let xhttp = new XMLHttpRequest();
//   xhttp.onreadystatechange = function () {
//     if (this.readyState == 4 && this.status == 200) {
//        return xhttp.responseText;
//     }
//   };
//   xhttp.open("GET", geojsonUrl);
//   xhttp.send();

// };

/*

export const fetchCurrentObservations = async (): Promise<[]> => {
  // URL

  // fetch('http://ip-api.com/json', { method: "GET", mode: 'cors', headers: { 'Content-Type': 'application/json',}}).then(response => response.json())

  return await fetch(geojsonUrl, {
    method: "GET",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw response;
      }
      if (response.status !== 204) {
        return response.json();
      } else {
        return null;
      }
    })
    .then((response) => {
      const retour = response.features.map((item: Feature) => {
        // TODO à typer
        return item;
      });
      return retour;
    });
};

*/
