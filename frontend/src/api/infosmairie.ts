import { City } from "@/types/city";
import { Feature } from "geojson";
// import { createPinia } from "pinia";

export const getInfosMairie_By_Code = async (params: any): Promise<[]> => {
    const codeINSEE = "01002"
    const Url = `https://etablissements-publics.api.gouv.fr/v3/communes/${codeINSEE}/mairie`;
    //params.type = ""
    return await fetch(Url, {
        method:"GET",
        headers: {
            "Content-Type": "application/json",
        }
    })
    .then((response) => {
        if (!response.ok) {
            throw response;
        }
        if (response.status !== 204) {
            return response.json();
        } else {
            return null;
        }
    })
    .then((response) => {
        const retour = response.features.map((item: Feature) => {
          const a = new City(item.properties);
          return a;
        });
        return retour;
      });
  };