/* eslint-disable @typescript-eslint/no-explicit-any */
import Papa from "papaparse";

export const fetchPrefectoralDecrees = () =>
  new Promise((resolve, reject) => {
    Papa.parse(process.env.BASE_URL + "data/dpts_lien_arrete.csv", {
      header: true,
      download: true,
      complete: function (results) {
        resolve(results.data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });

export const fetchPrefectoralDecreesFromSite = async (): Promise<any[]> => {
  return await fetch("https://ambroisie-risque.info/wp-json/wp/v2/pages/276")
    .then(function (response) {
      return response.json();
    })
    .then((json) => {
        console.log("zzz", new DOMParser().parseFromString(json.content.rendered, "text/html").querySelectorAll("tr"))
      return [];
      console.log(htmlTableToArray(json.content.rendered));
    });
};
export const htmlTableToArray = (html: string): Array<any> => {
  const props = [
    "prop1",
    "prop2",
    "prop3",
    "prop4",
    "prop5",
    "prop6",
    "prop7",
    "prop8",
  ];

  return [];
//   return Array.from(
//     new DOMParser().parseFromString(html, "text/html").querySelectorAll("tr"),
//     (row) =>
//       [...row.cells].reduce(
//         (o, cell, i: number) => ((o[props[i]] = cell.textContent), o),
//         {}
//       )
//   );
};
