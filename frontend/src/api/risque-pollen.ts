import Papa from "papaparse";

export const fetchRisquePollen = () =>
  new Promise((resolve, reject) => {
    Papa.parse(process.env.BASE_URL + "data/risque_pollen.csv", {
      header: true,
      download: true,
      complete: function (results) {
        resolve(results.data);
      },
      error: function (error) {
        reject(error);
      },
    });
  });